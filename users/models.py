from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import ugettext_lazy as _


class Skill(models.Model):
    """"Навыки исполнителя"""
    name = models.CharField(max_length=255, unique=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Skill')
        verbose_name_plural = _('Skills')


class Customer(models.Model):
    """
    Заказчик 
    """
    user = models.ForeignKey(User)

    def __str__(self):
        return self.user.get_full_name()

    class Meta:
        verbose_name = _('Customer')
        verbose_name_plural = _('Customers')


class Executor(models.Model):
    """
    Исполнитель
    """
    phone = models.CharField(max_length=20)
    user = models.ForeignKey(User)
    skills = models.ManyToManyField(Skill)
    avatar = models.ImageField(blank=True, null=True)

    def __str__(self):
        return self.user.get_full_name()

    class Meta:
        verbose_name = _('Executor')
        verbose_name_plural = _('Executors')
