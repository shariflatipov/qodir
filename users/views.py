from django.shortcuts import render

from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from rest_framework.response import Response

from users.utils import user_to_dict


class CustomObtainToken(ObtainAuthToken):
    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)
        client_dict = user_to_dict(user)
        return Response({
                'token': token.key, 
                'user': client_dict, 
                'status': {
                    'code':1, 
                    'msg': 'Пользователь успешно залогинился'
                 }
            })
