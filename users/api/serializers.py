from django.contrib.auth.models import User
from rest_framework import serializers

from users.models import (
    Customer,
    Executor,
    Skill,
)


class SkillSerializer(serializers.ModelSerializer):
    class Meta:
        model = Skill
        fields = '__all__'


class ExecutorSerializer(serializers.ModelSerializer):
    skills = serializers.SerializerMethodField()
    full_name = serializers.SerializerMethodField()
    phone = serializers.SerializerMethodField()
    email = serializers.SerializerMethodField()
    avatar = serializers.SerializerMethodField()

    def get_skills(self, obj):
        return obj.skills.all().values_list('name', flat=True)

    def get_full_name(self, obj):
        return obj.user.get_full_name()

    def get_phone(self, obj):
        return obj.phone

    def get_email(self, obj):
        return obj.user.email

    def get_avatar(self, obj):
        if obj.avatar:
            return obj.avatar
        else:
            return ''

    class Meta:
        model = Executor
        fields = '__all__'


class SignupSerializer(serializers.Serializer):
    """Регистрация нового пользователя"""
    full_name = serializers.CharField(max_length=20)
    phone = serializers.CharField(max_length=13)
    token = serializers.CharField(max_length=200)

    def validate(self, data):
        if User.objects.filter(username=data['phone']).exists():
            raise serializers.ValidationError("username {} is already exists".format(data['phone']))
        return data
