from django.http.response import Http404
from django.contrib.auth.models import User
from django.db import transaction

from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.generics import CreateAPIView

from users.models import (
    Executor
)

from users.utils import executor_to_dict 

from .serializers import (
    ExecutorSerializer,
    SignupSerializer
)


class ExecutorListView(APIView):

    permission_classes = (AllowAny,)

    def get(self, request):
        executors = Executor.objects.all()
        serializer = ExecutorSerializer(executors, many=True, read_only=True)
        return Response(serializer.data)


class ExecutorDetails(APIView):
    permission_classes = (AllowAny,)

    def get_object(self, pk):
        try:
            return Executor.objects.get(pk=pk)
        except:
            raise Http404

    def get(self, request, pk):
        executor = self.get_object(pk)
        serializer = ExecutorSerializer(executor)
        return Response(serializer.data)


class SignupView(CreateAPIView):
    """Регистрация пользователя"""
    serializer_class = SignupSerializer
    permission_classes = (AllowAny,)
    queryset = Executor.objects.none()  # todo Не понятно почему CreateAPIView требует queryset

    def post(self, request):
        try:
            signup = SignupSerializer(data=request.data)
            if signup.is_valid():
                with transaction.atomic():
                    vd = signup.validated_data

                    user = User()
                    client = Executor()
                    user.username = vd['phone']
                    if 'email' in vd:
                        user.email = vd['email']
                    user.first_name = vd['full_name']
                    user.save()

                    client.phone = vd['phone']
                    client.user = user
                    client.save()

                    token = Token.objects.create(user=user)

                    resp_data = {
                        "code": 1,
                        "msg": "Поздравляю, пользователь успешно создан!",
                        'token': token.key
                    }

                    resp_data.update(executor_to_dict(client))
                    return Response(resp_data, status=status.HTTP_201_CREATED)

            else:
                resp_data = {'status': {"code": -1, "msg": "error"}}
                resp_data = dict(dict(signup.errors), **resp_data)
                return Response(resp_data, status=status.HTTP_200_OK)

        except Exception as e:
            print(e)
            resp_data = {
                'status': {
                    'code': -1000,
                    'msg': 'Ошибка!'
                }
            }
        return Response(resp_data, status=status.HTTP_200_OK)

