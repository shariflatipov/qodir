from django.conf.urls import url

from .views import (
    ExecutorDetails,
    ExecutorListView,
    SignupView
)


urlpatterns = [
    url(r'^executor/(?P<pk>[0-9]+)', ExecutorDetails.as_view(), name='executor'),
    url(r'^executor/$', ExecutorListView.as_view(), name='executors'),
    url(r'^executor/create/$', SignupView.as_view(), name='create_executor'),
]
