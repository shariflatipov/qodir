from users.models import Customer, Executor


def user_to_dict(user):
    return {
        'id': user.pk,
        'name': user.user.get_full_name(),
        'email': user.user.email,
        'avatar': user.avatar,
        'phone': user.phone
    }


def customer_to_dict(user):
    client = Customer.objects.get(user=user)
    return {
        'id': client.pk,
        'name': client.user.get_full_name(),
    }


def executor_to_dict(user):
    avatar = ''
    if user.avatar:
        avatar = user.avatar
    return {
        'id': user.pk,
        'name': user.user.get_full_name(),
        'email': user.user.email,
        'avatar': avatar,
        'phone': user.phone
    }
