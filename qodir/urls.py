"""
    Base URL Configuration
"""
from django.conf.urls import url, include
from django.contrib import admin

from rest_framework import routers

from users.views import CustomObtainToken

router = routers.DefaultRouter()

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^projects/', include('projects.urls', namespace='projects')),
    url(r'^api/projects/', include('projects.api.urls', namespace='api-projects')),
    url(r'^api/users/', include('users.api.urls', namespace='api-users')),
    url(r'^users/', include('users.urls', namespace='users')),

    url(r'^api-token-auth/', CustomObtainToken.as_view()),

    url(r'^api-auth-session/',
        include('rest_framework.urls', namespace='rest_framework')),
]
