from django.db import models
from django.utils.translation import ugettext_lazy as _

from users.models import Executor, Customer


class ProjectType(models.Model):
    """
        Project type e.g 
        Automobile
        Remount
    """
    parent = models.ForeignKey("ProjectType", blank=True, null=True)
    code = models.CharField(max_length=244, unique=True)
    name = models.CharField(max_length=244)

    def __str__(self):
        return "{}: {}".format(self.code, self.name)

    class Meta:
        verbose_name = _('Project type')
        verbose_name_plural = _('Project types')


class Project(models.Model):
    """
    Concrete project(portfolio)
    """
    project_type = models.ForeignKey(ProjectType)
    name = models.CharField(max_length=255)
    date_start = models.DateField(null=True, blank=True)
    date_end = models.DateField(null=True, blank=True)
    executor = models.ForeignKey(Executor)
    description = models.TextField()

    def __str__(self):
        return "{}: {} - {}".format(self.project_type.name, self.name, self.description)

    class Meta:
        verbose_name = _("Project")
        verbose_name_plural = _("Projects")


class Gallery(models.Model):
    """
    Images of project
    """
    project = models.ForeignKey(Project)
    image = models.ImageField()
    description = models.CharField(max_length=200)

    def __str__(self):
        return "{} {}".format(self.project.name, self.description)

    class Meta:
        verbose_name = _("Image gallery")
        verbose_name_plural = _("Image galleries")


class ProjectComment(models.Model):
    """
    Comments for project
    """
    project = models.ForeignKey(Project)
    comment = models.CharField(max_length=1000)
    commentator = models.ForeignKey(Customer)

    class Meta:
        verbose_name = _("Project comment")
        verbose_name_plural = _("Project comments")

    def __str__(self):
        return "{} - {}".format(self.project.name, self.comment)


class Criteria(models.Model):
    """
    Mark criteria
    """
    project_type = models.ForeignKey(ProjectType)
    code = models.CharField(max_length=40, unique=True)
    name = models.CharField(max_length=244)
    coefficient = models.FloatField()

    def __str__(self):
        return "{}: {}".format(self.project_type.name, self.name)

    class Meta:
        verbose_name = _("Mark criteria")
        verbose_name = _("Mark criterias")


class ProjectMark(models.Model):
    """
    Marks of projects from Customers
    devided by criteria
    """
    ONE = 1
    TWO = 2
    THREE = 3
    FOUR = 4
    FIVE = 5
    MARKS = (
        (ONE, _('one')), 
        (TWO, _('two')),
        (THREE, _('three')),
        (FOUR, _('four')),
        (FIVE, _('five')),
    )
    project_comment = models.ForeignKey(ProjectComment)
    criteria = models.ForeignKey(Criteria)
    mark = models.IntegerField(choices=MARKS)

    def __str__(self):
        return "{} -{}".format(self.criteria, self.mark)

    class Meta:
        verbose_name = _('Project mark')
        verbose_name_plural = _('Project marks')
