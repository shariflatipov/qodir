from django.conf.urls import url

from .views import (
    CriteriaView,
    ProjectListAPIView,
)


urlpatterns = [
    url(r'^criteria/$', CriteriaView.as_view(), name='criteria'),
    url(r'^$', ProjectListAPIView.as_view(), name='projects'),
]
