from django.shortcuts import render

from rest_framework import generics
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import AllowAny

from projects.models import (
    Criteria,
    Gallery,
    Project,
    ProjectComment,
    ProjectMark,
    ProjectType,
)

from projects.api.serializers import (
    CriteriaSerializer,
    ProjectSerializer,
)

class CriteriaView(APIView):

    def get(self, request, format=None):
        criterias = Criteria.objects.all()
        serialized = CriteriaSerializer(criterias, many=True)
        return Response(serialized.data)

    def post(self, request, format=None):
        pass


class ProjectListAPIView(generics.ListAPIView):
    permission_classes = (AllowAny,)
    serializer_class = ProjectSerializer
    queryset = Project.objects.all()
    