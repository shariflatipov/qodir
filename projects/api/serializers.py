from rest_framework import serializers

from projects.models import (
    Criteria,
    Gallery,
    Project,
    ProjectComment,
    ProjectMark,
    ProjectType,
)

from users.api.serializers import SkillSerializer


class CriteriaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Criteria
        fields = '__all__'


class ProjectSerializer(serializers.ModelSerializer):
    executor = serializers.SerializerMethodField()
    skills = serializers.SerializerMethodField()

    def get_executor(self, obj):
        return {'pk': obj.pk, 'name': obj.executor.user.get_full_name()}

    def get_skills(self, obj):
        return SkillSerializer(obj.executor.skills.all(), many=True).data

    class Meta:
        model = Project
        fields = '__all__'
