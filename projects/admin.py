from django.contrib import admin

from .models import (
    Criteria,
    Gallery,
    Project,
    ProjectComment,
    ProjectMark,
    ProjectType
)

admin.site.register(Criteria)
admin.site.register(Gallery)
admin.site.register(Project)
admin.site.register(ProjectComment)
admin.site.register(ProjectMark)
admin.site.register(ProjectType)
