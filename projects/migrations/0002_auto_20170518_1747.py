# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-05-18 17:47
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('projects', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Criteria',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(max_length=40, unique=True)),
                ('name', models.CharField(max_length=244)),
                ('coefficient', models.FloatField()),
            ],
            options={
                'verbose_name': 'Mark criterias',
            },
        ),
        migrations.CreateModel(
            name='ProjectMark',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('mark', models.IntegerField(choices=[(1, 'one'), (2, 'two'), (3, 'three'), (4, 'four'), (5, 'five')])),
                ('criteria', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='projects.Criteria')),
            ],
            options={
                'verbose_name': 'Project mark',
                'verbose_name_plural': 'Project marks',
            },
        ),
        migrations.CreateModel(
            name='ProjectType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(max_length=244, unique=True)),
                ('name', models.CharField(max_length=244)),
                ('parent', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='projects.ProjectType')),
            ],
            options={
                'verbose_name': 'Project type',
                'verbose_name_plural': 'Project types',
            },
        ),
        migrations.AlterModelOptions(
            name='gallery',
            options={'verbose_name': 'Image gallery', 'verbose_name_plural': 'Image galleries'},
        ),
        migrations.AlterModelOptions(
            name='project',
            options={'verbose_name': 'Project', 'verbose_name_plural': 'Projects'},
        ),
        migrations.AlterModelOptions(
            name='projectcomment',
            options={'verbose_name': 'Project comment', 'verbose_name_plural': 'Project comments'},
        ),
        migrations.AddField(
            model_name='projectmark',
            name='project_comment',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='projects.ProjectComment'),
        ),
        migrations.AddField(
            model_name='criteria',
            name='project_type',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='projects.ProjectType'),
        ),
        migrations.AddField(
            model_name='project',
            name='project_type',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='projects.ProjectType'),
            preserve_default=False,
        ),
    ]
